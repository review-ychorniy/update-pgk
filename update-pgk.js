import 'dotenv/config'
import axios from 'axios';;
import chalk from 'chalk';
import qs from 'qs';

axios.interceptors.response.use((resp) => resp.data);

const config = {
    bitbucket: {
        workspace: "yaroslavert",
        repoSlug: "example",
        clientId: process.env.BITBUCKET_CLIENT_ID,
        secret: process.env.BITBUCKET_SECRET,
    },
    branch: 'pck',
}

class BitbucketSrc {
    access_token = null;
    _axios = null;

    constructor() {
        this._axios = axios.create({
            baseURL: 'https://api.bitbucket.org/2.0/',
        });
        this._axios.interceptors.request.use((config) => {
            if (this.access_token) {
                config.headers.Authorization = `Bearer ${this.access_token}`;
            }
            return config;
        });
        this._axios.interceptors.response.use((resp) => resp.data);
    }

    async requestAccessToken() {
        const { access_token } = await axios.post("https://bitbucket.org/site/oauth2/access_token", "grant_type=client_credentials", {
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            },
            auth: {
                username: config.bitbucket.clientId,
                password: config.bitbucket.secret
            }
        });

        this.access_token = access_token;
    }

    async getFile(workspace, repoSlug, commit, path) {
        return this._axios.get(`/repositories/${workspace}/${repoSlug}/src/${commit}/${path}`);
    }

    async getBranch(workspace, repoSlug, branch) {
        return this._axios.get(`/repositories/${workspace}/${repoSlug}/refs/branches/${branch}`);
    }

    async createBranch(workspace, repoSlug, branch, hash) {
        return this._axios.post(`/repositories/${workspace}/${repoSlug}/refs/branches`, {
            "name": branch,
            "target": {
                "hash": hash
            }
        });
    }

    async getOrCreateBranch(workspace, repoSlug, name, hash) {
        let branch;

        try {
            branch = await this.getBranch(workspace, repoSlug, name);
        } catch (err) {}

        if (!branch) {
            branch = await this.createBranch(workspace, repoSlug, name, hash);
        }

        return branch;
    }

    async createCommitByUploadFile(workspace, repoSlug, branchName, fileName, fileContent, message) {
        const formData = {
            branch: branchName,
            [fileName]: JSON.stringify(fileContent, null, 2)
        };

        if (message) {
            formData.message = message;
        }

        return this._axios.post(`/repositories/${workspace}/${repoSlug}/src`, qs.stringify(formData), {
            headers: {
                "content-type": "application/x-www-form-urlencoded"

            }
        });
        
    }

    async createPullRequest(workspace, repoSlug, title, sourceBranch, destinationBranch) {
        return this._axios.post(`/repositories/${workspace}/${repoSlug}/pullrequests`, {
            "title": title,
            "source": {
                "branch": {
                    "name": sourceBranch
                }
            },
            "destination": {
                "branch": {
                    "name": destinationBranch
                }
            }
        });
    }
}

async function findPkgTags(name) {
    return axios.get(`https://registry.npmjs.org/-/package/${name}/dist-tags`);
}

async function main() {
    const { bitbucket: btbConfig } = config;

    const bitbucketSrc = new BitbucketSrc();

    await bitbucketSrc.requestAccessToken();
    const pck = await bitbucketSrc.getFile(btbConfig.workspace, btbConfig.repoSlug, "master", "package.json");
    await bitbucketSrc.getOrCreateBranch(btbConfig.workspace, btbConfig.repoSlug, config.branch, "master");

    const dependencyKeys = ['devDependencies', 'dependencies'];

    for (let dependencyKey of dependencyKeys) {
        const dependencyLists = pck[dependencyKey] || {};

        console.log(chalk.blue(`${dependencyKey} ↓`));

        for (let [key, currentVersion] of Object.entries(dependencyLists)) {
            const { latest: latestVersion } = await findPkgTags(key);
    
            if (!currentVersion.includes(latestVersion)) {
                dependencyLists[key] = latestVersion;
                console.log(chalk.yellow(`update ${chalk.black(key)}:`, `${chalk.red(currentVersion)} -> ${chalk.green(latestVersion)}`))
            }
        }

        if (Object.keys(dependencyLists).length) {
            pck[dependencyKey] = dependencyLists;
        }
    }

    await bitbucketSrc
        .createCommitByUploadFile(
            btbConfig.workspace, btbConfig.repoSlug, config.branch,
            "package.json", pck, "Update package.json"
        );

    const { links: 
        { html: { href: pullRequestUrl }
    } } = await bitbucketSrc.createPullRequest(
        btbConfig.workspace, btbConfig.repoSlug, 
        "Update packages", config.branch, "master"
    );

    console.log(chalk.blueBright("Pull Request Created ->"), chalk.greenBright(pullRequestUrl));
}

main()
    .then(() => {})
    .catch((err) => {
        console.error('Failed scripts update', err.message);
    })