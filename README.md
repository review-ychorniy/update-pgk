# Package Json Updater

## Tech

Dependencies:

- [Axios] - http helper
- [Сhalk] - highlight for colors
- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework [@tjholowaychuk]
- [qs] - A querystring parsing and stringifying library

### Setup Config (replace config)

```sh
bitbucket 
    workspace
    repoSlug
    clientId
    secret
```
### Run script
```sh
node update-pgk.js
```